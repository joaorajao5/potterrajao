//
//  ViewController.swift
//  potterRajao
//
//  Created by COTEMIG on 27/10/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Ator: Decodable {
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeAtores: [Ator] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeAtores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MinhaCelula
        let ator = listaDeAtores[indexPath.row]
        
        cell.nome.text = ator.name
        cell.ator.text = ator.actor
        cell.imagem.kf.setImage(with: URL(string: ator.image))
        
        return cell
    }
    
    @IBOutlet var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        dados()
        TableView.dataSource = self
    }
    
    private func dados(){
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Ator].self){
            response in
            if let lista_ator = response.value{
                self.listaDeAtores = lista_ator
            }
            self.TableView.reloadData()
        }
    }
}

